﻿using Common.Lib.Core.Context;
using Common.Lib.Infrastructure;
using System;

namespace Common.Lib.Core
{
    public abstract class  Entity
    {
        public Guid Id { get; set; }

        public ValidationResult CurrentValidation { get; set; }

        public static IDependencyContainer DepCon { get; set; }

        public virtual ValidationResult Validate()
        {
            var output = new ValidationResult()
            {
                IsSuccess = true
            };

            return output;
        }

        public virtual ValidationResult ValidateDelete()
        {
            var output = new ValidationResult
            {
                IsSuccess = true
            };

            return output;
        }

        /* Exists()
        public abstract ValidationResult Exists();
        */

        public virtual SaveResult<T> Save<T>() where T : Entity
        {
            var output = new SaveResult<T>();

            CurrentValidation = Validate();

            if (CurrentValidation.IsSuccess)
            {
                //Aquí necesito la inyección de dependencias


                if (this.Id == Guid.Empty)
                {
                    this.Id = Guid.NewGuid();
                    //ResolveRepository<T>().Add(this as T);
                }
                else
                {
                    //ResolveRepository<T>().Update(this as T);
                    throw new Exception();
                }
            }

            output.Validation = CurrentValidation;

            return output;
        }

        public virtual SaveResult<T> Delete<T>() where T : Entity
        {
            var output = new SaveResult<T>();

            CurrentValidation = ValidateDelete();

            if (CurrentValidation.IsSuccess == true)
            {
                //ResolveRepository<T>().Delete(this as T);
                throw new Exception();
            }

            output.Validation = CurrentValidation;

            return output;
        }

        public virtual T Clone<T>() where T : Entity, new()
        {
            var output = new T();
            output.Id = this.Id;
            return output;
        }
    }
}
