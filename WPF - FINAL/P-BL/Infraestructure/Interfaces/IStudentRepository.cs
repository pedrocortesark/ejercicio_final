﻿using Common.Lib.Core.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace P_BL.Infraestructure.Interfaces
{
    public interface IStudentRepository : IRepository<Student>
    {
        Student GetStudentByDni(string dni);
    }
}
