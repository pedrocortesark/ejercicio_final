﻿using Common.Lib.Core.Context;
using Common.Lib.DAL.EFCore;
using Common.Lib.Infrastructure;
using P___DALCliente.Context;
using P_BL;
using P_BL.Infraestructure.Interfaces;

namespace WPF___FINAL.Infraestructure
{
    public class Bootstrapper
    {
        public Bootstrapper()
        {

        }

        public void Init(IDependencyContainer dp)
        {
            RegisterRepositories(dp);
        }

        void RegisterRepositories(IDependencyContainer dp)
        {
            dp.Register<IRepository<Student>, StudentClientRepository>();
            dp.Register<IStudentRepository, StudentClientRepository>();
        }
    }
}

