﻿using Common.Lib.Core;
using Common.Lib.Infrastructure;
using Infraestructure;
using Infraestructure.Commands;
using P_BL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Input;
using WPF___FINAL.Infraestructure;

namespace ViewModels
{
    public class StudentVM : GenericView
    {

        #region Propiedades!

        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaisePropertyChanged("CurrentStudent");
            }
        }

        private string _dni;

        public string Dni
        {
            get
            {
                return _dni;
            }
            set
            {
                _dni = value;
                RaisePropertyChanged("CurrentStudent");
            }
        }

        private string _chair;

        public string Chair
        {
            get
            {
                return _chair;
            }
            set
            {
                _chair = value;
                RaisePropertyChanged("CurrentStudent");
            }
        }

        #endregion

        private ObservableCollection<Student> _students = new ObservableCollection<Student>();

        public ObservableCollection<Student> Students
        {
            get 
            { 
                return _students; 
            }
            set 
            { 
                _students = value;
                RaisePropertyChanged("Students");
            }
        }


        private Student _currentStudent;

        public Student CurrentStudent
        {
            get 
            { 
                return _currentStudent; 
            }
            set 
            { 
                _currentStudent = value;
                RaisePropertyChanged("CurrentStudent");
            }
        }



        private ICommand _addStudentCommand;

        public ICommand AddStudentCommand
        {
            get
            {
                if (_addStudentCommand == null)
                    _addStudentCommand = new RelayCommand(new Action(CreateStudent));

                return _addStudentCommand;
            }
        }
           


        private void CreateStudent()
        {
            var bootstrapper = new Bootstrapper();
            Entity.DepCon = new SimpleDependencyContainer();
            bootstrapper.Init(Entity.DepCon);


            
            ValidationResult<string> vrName = Student.ValidateName(this.Name);
            ValidationResult<string> vrDni = Student.ValidateDni(this.Dni);
            ValidationResult<int> vrChair = Student.ValidateChair(this.Chair);

            if (vrDni.IsSuccess && vrName.IsSuccess && vrChair.IsSuccess)
            {
                var newStudent = new Student()
                {
                    Name = vrName.ValidatedResult,
                    Dni = vrDni.ValidatedResult,
                    Chair = vrChair.ValidatedResult
                };
                Students.Add(newStudent);
            }
            else
            {
                var errors = string.Empty;
                errors += vrName.AllErrors + "\n\r";
                errors += vrDni.AllErrors + "\n\r";
                errors += vrChair.AllErrors + "\n\r";
                MessageBox.Show(errors);
            }
                
        }

    }
}
